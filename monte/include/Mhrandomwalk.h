#pragma once
#include<iostream>  
#include<random>  
#include<time.h>  
#include "Geomtry.h"
#include <vector>



/*
非常自然的，我们选PMMA的下表面的中心点作为(0,0,0);
这样的建模将导致后面的Transport模块中的计算是方便的
*/

//考虑到当前分布相当简单，因而我们使用简单的抽样方法，而不是
//随机行走
//要生成三种随机数
//1.在一个圆上的均匀分布，用来作为光源的位置
//2.0到2pi的均匀分布，用于给定phi角
//3.0到pi/2，正比于cos(pi)的一个分布


using std::vector;
//目的，创建一个类，他便和三个分布绑定，分别调用各自的Get
//方法，可以得到一个随机数。
class Mhrandomwalk
{
private:
	//geomtry
	Geomtry* m_geo;

public:
	Mhrandomwalk();
	virtual ~Mhrandomwalk();

	//在一个圆上的均匀分布
	//方案，在一个正方形上生成均匀分布随机数。
	//判断是否在圆内，是的话就接受

private:
	//第一个数为x，第二个数为y,将在构造函数处初始化
	vector<double> m_Circle_xy;
public:
	virtual vector<double> Get_Circle();

	//0到2pi的均匀分布
private:
	vector<double> m_phi;

public:
	vector<double> Get_Circle_in();
	virtual vector<double> Get_phi();

private:
	vector<double> m_theta;
	//这是cos(x)密度分布对应的概率分布函数，很自然的，这便是sin(x)，
	//而且这刚刚好是归一化的
	//这个函数用不到，写在这里只是为了完整
	virtual double Func(double x);
	//抽样需要用到的Func的反函数;
	virtual double Func_1(double x);

public:
	vector<double>& Get_old_theta()
	{
		return m_theta;
	}

	
public:
	//似乎有边界效应
	//有边界效应，导致在0附近的生成数量变小
	//现在考虑一种改进方法：我们做从-pi/2到pi/2的抽样
	virtual vector<double> Get_theta();

	

};

