#include "Transport.h"
#include"Geomtry.h"
#include <random>
#include <iostream>



double Transport::GetRefractionAngle_2()
{
	double incident_angle = m_theta[0];

	double n_PMMA = geo->Get_n_PMMA();
	double n_air = geo->Get_n_air();

	//use refraction law

	double sin_theta2 = std::sin(m_theta[0]) * n_air / n_PMMA;
	if (sin_theta2 > 1)
		return 3.1415926 / 2;

	return std::asin(sin_theta2);
}


void Transport::Set_Retangular()
{
	//set position
	//m_position[0] = m_Circle_xy[0];
	//m_position[1] = m_Circle_xy[1];
	//m_position[2] = 0.0;

	//std::cout << "111" << std::endl;
	//set direction
	//now its Module is 1
	//std::cout << "111" << std::endl;
	m_direction[0] = 1.0 * std::sin(m_theta[0]) * std::cos(m_phi[0]);
	m_direction[1] = 1.0 * std::sin(m_theta[0]) * std::sin(m_phi[0]);
	m_direction[2] = 1.0 * std::cos(m_theta[0]);
}

void Transport::Set_Circle_xy(vector<double>& vec)
{
	m_Circle_xy[0] = vec[0];
	m_Circle_xy[1] = vec[1];

	//innitialize position
	m_position[0] = m_Circle_xy[0];
	m_position[1] = m_Circle_xy[1];
	m_position[2] = 0.0;
}

void Transport::Set_phi(vector<double>& vec)
{
	m_phi[0] = vec[0];
}

void Transport::Set_theta(vector<double>& vec)
{
	m_theta[0] = vec[0];
	m_theta[0] = GetRefractionAngle_2();
	m_angle_in_PMMA[0] =m_theta[0];
}

void Transport::Transport_layer1()
{	
	//update retangular 
	this -> Set_Retangular();
	double thickness = geo->Get_PMMA_thickness();
	
	//只需要沿出射方向乘以 z / thickness,即可得移动的向量
	double layer1_factor = thickness / m_direction[2];

	//update the position
	m_position[0] = m_position[0] + m_direction[0] * layer1_factor;
	m_position[1] = m_position[1] + m_direction[1] * layer1_factor;
	m_position[2] = m_position[2] + m_direction[2] * layer1_factor;

	//GetRefraction Angle and update it
	auto ref_angle = GetRefractionAngle();
	//update
	m_theta[0] = ref_angle;
	//set retangular
	//update the retangular members
	this -> Set_Retangular();

}

void Transport::Transport_layer2()
{
	double thickness = geo->Get_Shell_h();

	//只需要沿出射方向乘以 z / thickness,即可得移动的向量
	double layer1_factor = thickness / m_direction[2];

	//update the position 
	m_position[0] = m_position[0] + m_direction[0] * layer1_factor;
	m_position[1] = m_position[1] + m_direction[1] * layer1_factor;
	m_position[2] = m_position[2] + m_direction[2] * layer1_factor;

}

int Transport::Hit_Detection()
{	
	//
	double active_a = geo->Get_Active();
	
	if (m_position[0] > active_a / 2)
	{
		//it means not hit
		return 0;
	}
	else if (m_position[1] > active_a / 2)
	{
		//it means not hit
		return 0;
	}
	/*else if (isnan(m_position[0]))
		return 0;
	else if (isnan(m_position[1]))
		return 0;
	else if (isnan(m_position[2]))
		return 0;*/
	else
		//it means hit
		return 1;
	
}

double Transport::GetRefractionAngle()
{	
	double incident_angle = m_theta[0];

	double n_PMMA = geo->Get_n_PMMA();
	double n_air = geo->Get_n_air();

	//use refraction law

	double sin_theta2 = std::sin(m_theta[0]) * n_PMMA / n_air;
	if (sin_theta2 > 1)
		return 3.1415926/2;

	return std::asin(sin_theta2);
}

int Transport::Run_Once()
{	
	//std::cout << "RUN　ONCE" << std::endl;
	//check
	//如果入射角度很接近于pi/2,本就几乎不可能打到有效面积处
	//而且会导致程序出现一些奇异性的问题，我们决定直接舍弃这些。
	if (std::abs((m_theta[0] - 3.1415926 / 2)) < 0.0001)
	{	
		//cout << "return!!";
		return 0;
	}

	
	//first layer
	//it will update position and direction
	//std::cout << "layer1" << std::endl;
	this->Transport_layer1();

	auto window_R = geo->Get_SiPM_window_R();
	//如果现在已经在圈外，那么根本不用再计算
	if ((m_position[0] * m_position[0] + m_position[1] * m_position[1]) >= window_R * window_R)
	{
		return 0;
	}
	if (std::abs(m_theta[0] - 3.1415926/2) <= 0.0001)
	{
		return 0;
	}

	Caculate_T_a_factor();
	//second layer
	//it will update position and direction
	//std::cout << "layer2" << std::endl;
	this->Transport_layer2();
	return this->Hit_Detection();
}

void Transport::Caculate_T_a_factor()
{
	//using std for 三角函数
	using std::sin;
	using std::cos;
	using std::tan;

	//第一步，计算吸收的影响
	auto attenuation_length = geo->Get_Attenuation_length();
	auto asorb = 
		std::exp(-std::sqrt(m_position[2] * m_position[2] 
		+ m_position[0] * m_position[0] 
		+ m_position[1] * m_position[1]) / attenuation_length);
	

	auto PMMA_angle = m_angle_in_PMMA[0];
	auto air_angle = m_theta[0];

	auto rp = tan(air_angle - PMMA_angle) / tan(air_angle + PMMA_angle);
	auto rs = sin(air_angle - PMMA_angle) / sin(air_angle + PMMA_angle);


	
	

	
	auto factor = asorb * (1 - rp * rp / 2 - rs * rs / 2) * (1 - rp * rp / 2 - rs * rs / 2);
	//假定光是各向同性的，很自然的

	m_T_absorb_factor = factor;
}

double Transport::Get_T_a_factor()
{
	return m_T_absorb_factor;
}

Transport::Transport()
{
	geo = new Geomtry();
	m_Circle_xy.resize(2, 0.0);
	m_phi.resize(1, 0.0);
	m_theta.resize(1, 0.0);

	//protected
	m_Event_num = 1000;
	m_Hit_num = 0;

	//init position and direction vector
	m_position.resize(3, 0.0);
	m_direction.resize(3, 0.0);

	//asorb and refraction
	m_T_absorb_factor = 0.0;
	m_angle_in_PMMA.resize(1, 0.0);

}

Transport::~Transport()
{
	delete geo;
}
