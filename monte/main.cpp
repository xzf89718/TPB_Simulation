﻿// MonteCarloFORTPB.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include "Geomtry.h"
#include "Mhrandomwalk.h"
#include "Transport.h"
#include "Transport_incident.h"
#include<vector>

int main()
{
    std::cout << "Hello World!\n";
	//random number generator
	auto random_generator = new Mhrandomwalk();
	auto transport_manager = new Transport();

	int num_event = 100000;
	int num_hit = 0;
	vector<double> T_array;
	int times = 10;
	double* a, * b, * c;
	a = new double[times];
	b = new double[times];
	c = new double[times];

	auto circle = random_generator->Get_Circle();
	transport_manager->Set_Circle_xy(circle);

	auto phi = random_generator->Get_phi();
	transport_manager->Set_phi(phi);

	auto theta = random_generator->Get_theta();
	transport_manager->Set_theta(theta);
	double hit_T = 0;
	auto hit_or_not = transport_manager->Run_Once();
	for (int j = 0;j < times;j++) {
		
		if (j % (int)(times * 0.1) == 0)
			std::cout << ((double)j / (double)times) << std::endl;

		for (int i = 0;i < num_event;i++)
		{
			circle = random_generator->Get_Circle();
			transport_manager->Set_Circle_xy(circle);

			phi = random_generator->Get_phi();
			transport_manager->Set_phi(phi);

			theta = random_generator->Get_theta();
			transport_manager->Set_theta(theta);
			hit_or_not = transport_manager->Run_Once();

			if (hit_or_not == 1)
			{
				//std::cout << hit_or_not << std::endl;
				num_hit = num_hit + 1;
				T_array.push_back(transport_manager->Get_T_a_factor());
				//std::cout << transport_manager->Get_T_a_factor() << std::endl;
			}
		}
		//std::cout << "hit:" << num_hit << std::endl;
		//)_std::cout << T_array.size() << std::endl;
		for (int i = 0;i < T_array.size();i++)
			hit_T = hit_T + T_array[i];
		a[j] = hit_T;
		T_array.resize(0);
		//std::cout << "hit_T:" << hit_T << std::endl;

	}
	auto transport_manager_in = new Transport_incident();
	
	auto geo = new Geomtry();
	auto window_r = geo->Get_SiPM_window_R();
	auto PMMA_r = geo->Get_PMMA_R();
	double k = window_r * window_r / (PMMA_r * PMMA_r);
	
	int num_event_in = num_event;
	num_event_in = num_event_in * k;
	int num_hit_in = 0;


	auto circle_in = random_generator->Get_Circle_in();
	transport_manager_in->Set_Circle_xy(circle);

	auto phi_in = random_generator->Get_phi();
	transport_manager_in->Set_phi(phi);

	auto theta_in = random_generator->Get_theta();
	transport_manager_in->Set_theta(theta);

	hit_or_not = transport_manager_in->Run_Once();
	for (int j = 0;j < times;j++) {
		for (int i = 0;i < num_event_in;i++)
		{
			circle_in = random_generator->Get_Circle_in();
			transport_manager_in->Set_Circle_xy(circle_in);

			phi_in = random_generator->Get_phi();
			transport_manager_in->Set_phi(phi_in);

			theta_in = random_generator->Get_theta();
			transport_manager_in->Set_theta(theta_in);
			hit_or_not = transport_manager_in->Run_Once();

			if (hit_or_not == 1)
			{
				//std::cout << hit_or_not << std::endl;
				num_hit_in = num_hit_in + 1;
			}
		}
		//std::cout << "hit_in:" << num_hit_in << std::endl;

		//num_hit_in = num_hit_in * k * num_hit_in / num_event_in;
		//std::cout << "hit_in:" << num_hit_in << std::endl;
		//delete 
		b[j] = num_hit_in;
	}


	std::cout << "**********************************" << std::endl;
	for (int i = 0;i < times;i++)
		c[i] = a[i] / b[i];
	for (int i = 0;i < times;i++)
		std::cout << c[i] << std::endl;
	delete[] a;
	delete[] b;
	delete[] c;
	delete random_generator;
	delete transport_manager;

	delete transport_manager_in;
	return 0;
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门使用技巧: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
