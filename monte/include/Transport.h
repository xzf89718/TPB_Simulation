#pragma once

#include <iostream>
#include <cmath>
#include <vector>
#include "Geomtry.h"

/*
	这个类需要探测器的几何，折射率等相关参数。
	通过接口接收MH方法产生的随机方向的光束，然后进行计算，
	通过接口发送计算结果，1表示集中，0表示没有击中。
	这个版本将随机数发生器和光子的运动分开，实际上可以整合到一起。

	坐标系：
	以PMMA底部为原点
*/
using std::vector;
using std::cout;
using std::endl;
class Transport
{
	//固有成员
protected:
	//geo
	Geomtry* geo;

public:
	//default constructor
	Transport();
	//default destructor;
	virtual ~Transport();


	//这些成员将在后面的派生版本中使用
protected:
    int m_Event_num;
	int m_Hit_num;

	//通过接口从外界获得的随机数
protected:
	//random number
	vector<double> m_Circle_xy;
	vector<double> m_phi;
	vector<double> m_theta;



protected:
	//我们在程序里使用直角坐标系工作
	vector<double> m_position;
	vector<double> m_direction;
	
protected:
	double GetRefractionAngle_2();
	//Set method for direction
	//this method should run after Set 3 vector
	void Set_Retangular();
	
	
public:
	//Set method for 3 vector
	void Set_Circle_xy(vector<double>& vec);
	void Set_phi(vector<double>& vec);
	void Set_theta(vector<double>& vec);

protected:
	virtual void Transport_layer1();
	virtual void Transport_layer2();
	//0 -> not hit 1->hit
	int Hit_Detection();

protected:
	double GetRefractionAngle();

public:
	virtual int Run_Once();


private:
	double m_T_absorb_factor;
	vector<double> m_angle_in_PMMA;
	//这个方法应该在第一层计算完成，且更新了theta后
	void Caculate_T_a_factor();

public:
	double Get_T_a_factor();


};

