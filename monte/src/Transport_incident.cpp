#include "Transport_incident.h"
#include <random>
int Transport_incident::Run_Once()
{	

	if (std::abs((m_theta[0] - 3.1415926 / 2)) < 0.0001)
	{
		//cout << "return!!";
		return 0;
	}
	Transport_layer2();

	return Hit_Detection();
}

void Transport_incident::Transport_layer2()
{	
	//std::cout << "22222" << std::endl;
	//update retangular 
	this->Set_Retangular();
	double thickness = geo->Get_Shell_h();

	//只需要沿出射方向乘以 z / thickness,即可得移动的向量
	double layer1_factor = thickness / m_direction[2];

	//update the position
	m_position[0] = m_position[0] + m_direction[0] * layer1_factor;
	m_position[1] = m_position[1] + m_direction[1] * layer1_factor;
	m_position[2] = m_position[2] + m_direction[2] * layer1_factor;

	

}
