#pragma once

//这个类存放着整个几何的参数
//我们考虑使用厘米作为单位
class Geomtry
{
private:
	double m_PMMA_R;
	double m_PMMA_thickness;
	//refraction
	double m_n_PMMA;
	double m_n_air;

	//SiPM
	double m_SiPM_window_R;
	double m_Active;
	double m_Shell_h;
	
	//PMMA refraction and asorbtion
	//it means 100 cm -> 1m
	double m_attenuation_lenght = 100;

public:
	//default construtor
	//现有的尺寸
	Geomtry()
	{
		//PMMA片的参数
		m_PMMA_R = 2.83;
		m_PMMA_thickness = 0.283;
		//refraction
		m_n_PMMA = 1.483;
		m_n_air = 1;

		//SiPM
		m_SiPM_window_R = 0.5;
		m_Active = 0.58;
		m_Shell_h = 0.3;
	}
	//Get method
	double Get_PMMA_R()
	{
		return m_PMMA_R;
	}
	
	double Get_PMMA_thickness()
	{
		return m_PMMA_thickness;
	}

	double Get_n_PMMA()
	{
		return m_n_PMMA;
	}

	double Get_n_air()
	{
		return m_n_air;
	}

	double Get_SiPM_window_R()
	{
		return m_SiPM_window_R;
	}

	double Get_Active()
	{
		return m_Active;
	}

	double Get_Shell_h()
	{
		return m_Shell_h;
	}

	double Get_Attenuation_length()
	{
		return m_attenuation_lenght;
	}
};


