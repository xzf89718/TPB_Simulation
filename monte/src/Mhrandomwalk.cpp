#include <iostream>
#include <stdlib.h>
#include <time.h> 
#include<random>
#include<vector>
#include<cmath>

#include "Mhrandomwalk.h"
#include "Geomtry.h"
#include "Transport.h"

using std::vector;
using std::cout;
using std::endl;
Mhrandomwalk::Mhrandomwalk()
{	
	//use default constructor
	m_geo = new Geomtry();
	//innitialize to 0.0
	m_Circle_xy.resize(2, 0.0);
	m_phi.resize(1, 0.0);
	m_theta.resize(1, 0.0);
}

Mhrandomwalk::~Mhrandomwalk()
{
	delete m_geo;
}

vector<double> Mhrandomwalk::Get_Circle()
{
	double r = m_geo->Get_PMMA_R();
	//r = m_geo->Get_SiPM_window_R();
	//均匀分布
	//在函数中定义的随机数发生器，应该生命为stastic
	static std::default_random_engine random1(time(NULL));
	static std::uniform_real_distribution<double> dis1(-r, r);

	//auto random = new std::default_random_engine (time(NULL));
	//auto dis1 = new std::uniform_real_distribution<double> (-r, r);

	double norm = r * r + 1;
	double x, y;
	while (norm > r * r)
	{
		x = dis1(random1);
		y = dis1(random1);
		norm = x * x + y * y;
		
	}
	m_Circle_xy[0] = x;
	m_Circle_xy[1] = y;
	
	
	return m_Circle_xy;
}

vector<double> Mhrandomwalk::Get_Circle_in()
{
	double r = m_geo->Get_SiPM_window_R();
	//r = m_geo->Get_SiPM_window_R();
	//均匀分布
	//在函数中定义的随机数发生器，应该生命为stastic
	static std::default_random_engine random1_in(time(NULL));
	static std::uniform_real_distribution<double> dis1_in(-r, r);

	//auto random = new std::default_random_engine (time(NULL));
	//auto dis1 = new std::uniform_real_distribution<double> (-r, r);

	double norm = r * r + 1;
	double x, y;
	while (norm > r* r)
	{
		x = dis1_in(random1_in);
		y = dis1_in(random1_in);
		norm = x * x + y * y;

	}
	m_Circle_xy[0] = x;
	m_Circle_xy[1] = y;


	return m_Circle_xy;
}
vector<double> Mhrandomwalk::Get_phi()
{
	static std::default_random_engine random2(time(NULL));
	static std::uniform_real_distribution<double> dis2(0.0, 2 * 3.1415926);

	m_phi[0] = dis2(random2);
	return m_phi;
}

double Mhrandomwalk::Func(double x)
{
	return std::sin(x);
}

double Mhrandomwalk::Func_1(double x)
{
	return std::asin(x);
}


vector<double> Mhrandomwalk::Get_theta()
{	
	static std::default_random_engine random3(time(NULL));
	static std::uniform_real_distribution<double> dis3(-1.0, 1.0);

	//生成一个0到1的随机数
	double yita = dis3(random3);
	m_theta[0] = std::abs(Func_1(yita));
	
	return m_theta;
}
